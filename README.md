[![Payzos logo](/.gitassets/img/logo.png "Payzos logo")](https://payzos.io)

# Payzos-Prestashop

[Payzos](https://payzos.io) is payment service for Tezos network.

# Installation

## Pre release (git)

```
git clone https://gitlab.com/payzos/payzos-prestashop.git  PRESTASHOP_BASE_DIR/modules/payzos
```

and if you want to install on remote host. just clone from git and make a zip of plugin folder(don't forget to change folder name to payzos). then you can upload and install it to prestashop.\

Also you can download last realease zip file from [gitlab repository release page](https://gitlab.com/payzos/payzos-prestashop/-/releases)

## PrestaShop plugin installation

download last release zip file of plugin from  [gitlab repository release page](https://gitlab.com/payzos/payzos-prestashop/-/releases) and upload it from `admin panel > modules > Upload a module`

## Prestashop Marketplace

coming soon

# plugin configuration

-   in `admin_panel > modules >  payzos > setting` add your **wallet hash**
-   make **Live mode** yes

# support
- [gitlab issues](https://gitlab.com/payzos/payzos-prestashop/-/issues)
- info@payzos.io
- [payzos.io](https://payzos.io)
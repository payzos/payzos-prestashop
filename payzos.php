<?php
/**
 * 2007-2020 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 *  @author    Payzos <info@payzos.io>
 *  @copyright 2020 Payzos
 *  @license   https://gitlab.com/payzos/payzos-prestashop/-/blob/master/LICENSE  GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 */


require_once __DIR__ . '/classes/PayzosApi.php';

 /*
  * [description]
  */
if (!defined('_PS_VERSION_')) {
    exit();
}

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

/**
 * [Description Payzos]
 */
class Payzos extends PaymentModule
{
    /**
     * @var bool
     */
    protected $config_form = false;
    /**
     * @var string
     */
    public $wallet_hash;

    public function __construct()
    {
        $this->name = 'payzos';
        $this->tab = 'payments_gateways';
        $this->version = '1.0.0';
        $this->author = 'Payzos';
        $this->controllers = ['payment', 'validation'];
        $this->module_key = 'e6d5d0fe2af2eab4d7d20b0995101e50';
        $this->currencies = true;
        $this->currencies_mode = 'checkbox';

        $this->wallet_hash = Configuration::get('PAYZOS_WALLET_HASH');
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->trans('Payzos', [], 'Modules.Payzos.Admin');
        $this->description = $this->trans(
            'Payzos is Tezos payment service',
            [],
            'Modules.Payzos.Admin'
        );
        $this->ps_versions_compliancy = ['min' => '1.7', 'max' => _PS_VERSION_];
        $this->confirmUninstall = $this->trans('', [], 'Modules.Payzos.Admin');
        if (!isset($this->wallet_hash) || empty($this->wallet_hash)) {
            $this->warning = $this->trans(
                'The Wallet hash field must be configure before using this module',
                [],
                'Modules.Payzos.Admin'
            );
        }
        if (!count(Currency::checkPaymentCurrencies($this->id))) {
            $this->warning = $this->trans(
                'No currency has been set for this module.',
                [],
                'Modules.Payzos.Admin'
            );
        }
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     *
     * @return bool
     */
    public function install()
    {
        if (extension_loaded('curl') == false) {
            $this->_errors[] = $this->trans(
                'You have to enable the cURL extension on your server to install this module',
                [],
                'Modules.Payzos.Admin'
            );

            return false;
        }

        Configuration::updateValue('PAYZOS_LIVE_MODE', false);

        return parent::install() &&
            $this->registerHook('paymentOptions') &&
            $this->registerHook('paymentReturn') &&
            $this->registerHook('displayPaymentReturn');
    }

    /**
     * @return bool
     */
    public function uninstall()
    {
        Configuration::deleteByName('PAYZOS_LIVE_MODE');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     *
     * @return string(HTML)
     */
    public function getContent()
    {
        /*
         * If values have been submitted in the form, process.
         */
        if (((bool) Tools::isSubmit('submitPayzosModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        // $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');
        $output = '';

        return $output . $this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     *
     * @return string(HTML)
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitPayzosModule';
        $helper->currentIndex =
            $this->context->link->getAdminLink('AdminModules', false) .
            '&configure=' .
            $this->name .
            '&tab_module=' .
            $this->tab .
            '&module_name=' .
            $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = [
            'fields_value' => $this->getConfigFormValues() /* Add values for your inputs */,
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        ];

        return $helper->generateForm([$this->getConfigForm()]);
    }

    /**
     * Create the structure of your form.
     *
     * @return array
     */
    protected function getConfigForm()
    {
        return [
            'form' => [
                'legend' => [
                    'title' => $this->trans('Settings', [], 'Modules.Payzos.Admin'),
                    'icon' => 'icon-cogs',
                ],
                'input' => [
                    [
                        'type' => 'switch',
                        'label' => $this->trans('Live mode', [], 'Modules.Payzos.Admin'),
                        'name' => 'PAYZOS_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->trans(
                            'Use this module in live mode',
                            [],
                            'Modules.Payzos.Admin'
                        ),
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->trans('Enabled', [], 'Modules.Payzos.Admin'),
                            ],
                            [
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->trans('Disabled', [], 'Modules.Payzos.Admin'),
                            ],
                        ],
                    ],
                    [
                        'col' => 4,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-credit-card"></i>',
                        'desc' => $this->trans(
                            'Enter Wallet Public Key',
                            [],
                            'Modules.Payzos.Admin'
                        ),
                        'name' => 'PAYZOS_WALLET_HASH',
                        'label' => $this->trans('Wallet Hash', [], 'Modules.Payzos.Admin'),
                    ],
                ],
                'submit' => [
                    'title' => $this->trans('Save', [], 'Modules.Payzos.Admin'),
                ],
            ],
        ];
    }

    /**
     * Set values for the inputs.
     *
     * @return array
     */
    protected function getConfigFormValues()
    {
        return [
            'PAYZOS_LIVE_MODE' => Configuration::get('PAYZOS_LIVE_MODE', true),
            'PAYZOS_WALLET_HASH' => Configuration::get('PAYZOS_WALLET_HASH'),
        ];
    }

    /**
     * Save form data.
     *
     * @return void
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
     * Return payment options available for PS 1.7+
     *
     * @param array Hook parameters
     *
     * @return array|null
     */
    public function hookPaymentOptions($params)
    {
        if (!$this->active) {
            return;
        }
        if (!$this->checkCurrency($params['cart'])) {
            return;
        }
        $option = new PaymentOption();
        $option
            ->setCallToActionText($this->trans('Payzos', [], 'Modules.Payzos.Admin'))
            ->setAction($this->context->link->getModuleLink($this->name, 'validation', [], true));

        return [$option];
    }

    /**
     * @param CartCore $cart
     *
     * @return bool
     */
    public function checkCurrency($cart)
    {
        $currency_order = new Currency($cart->id_currency);
        $currencies_module = $this->getCurrency($cart->id_currency);
        if (is_array($currencies_module)) {
            foreach ($currencies_module as $currency_module) {
                if ($currency_order->id == $currency_module['id_currency']) {
                    return true;
                }
            }
        }

        return false;
    }
}

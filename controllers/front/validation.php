<?php
/**
 * 2007-2020 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 *  @author    Payzos <info@payzos.io>
 *  @copyright 2020 Payzos
 *  @license   https://gitlab.com/payzos/payzos-prestashop/-/blob/master/LICENSE  GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 */

/**
 * [PayzosValidationModuleFrontController description]
 */
class PayzosValidationModuleFrontController extends ModuleFrontController
{
    /**
     * @return void (but echo Json then exit program)
     */
    public function postProcess()
    {
        /*
         * If the module is not active anymore, no need to process anything.
         */
        if ($this->module->active == false) {
            die($this->trans('This payment method is not available.', [], 'Modules.Payzos.Shop'));
        }
        $cart = $this->context->cart;

        if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 ||
         !$this->module->active) {
            Tools::redirect('index.php?controller=order&step=1');
        }

        $customer = new Customer($cart->id_customer);

        if (!Validate::isLoadedObject($customer)) {
            Tools::redirect('index.php?controller=order&step=1');
        }
        if (empty(Configuration::get('PAYZOS_WALLET_HASH'))) {
            die($this->trans('Tezos Wallet Hash is not valid. Please contact to Admin', [], 'Modules.Payzos.Shop'));
        }
        $wallet_hash = Configuration::get('PAYZOS_WALLET_HASH');

        $currency = $this->context->currency;
        $total = (float) $cart->getOrderTotal(true, Cart::BOTH);
        $currency = (string) $currency->iso_code;
        $back_url = $this->context->link->getModuleLink('payzos', 'confirmation', ['cart_id' => $cart->id]);
        $payzos = new PayzosApi();
        $payment = $payzos->makePayment($wallet_hash, $total, $currency, $back_url);
        if (!$payzos) {
            die($this->trans('Unable to connect with payzos. Contact to Admin', [], 'Modules.Payzos.Shop'));
        }
        Tools::redirect($payment['url']);
    }
}

<?php
/**
 * 2007-2020 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 *  @author    Payzos <info@payzos.io>
 *  @copyright 2020 Payzos
 *  @license   https://gitlab.com/payzos/payzos-prestashop/-/blob/master/LICENSE  GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 */

/**
 * [PayzosConfirmationModuleFrontController description]
 */
class PayzosConfirmationModuleFrontController extends ModuleFrontController
{
    /**
     * @var PayzosApi
     */
    private $api;
    /**
     * @var bool
     */

    /**
     * @param string $_message
     *
     * @return void (but echo Json then exit program)
     */
    private function errorOutput($_message)
    {
        header('Content-type: application/json');
        header('Access-Control-Allow-Origin: *');

        $output = [];
        $output['ok'] = false;
        $output['message'] = $_message;
        echo json_encode($output);
        die();
    }

    public function postProcess()
    {
        $data = Tools::file_get_contents('php://input');
        if (empty($data) || !is_string($data)) {
            $this->errorOutput('unvalid request');
        }
        $data = json_decode($data, true);
        if (JSON_ERROR_NONE !== json_last_error()) {
            $this->errorOutput('unvalid input');
        }
        if (!isset($data['payment_id']) || !Tools::getIsset('cart_id')) {
            $this->errorOutput('fill values');
        }
        $payment_id = filter_var($data['payment_id'], FILTER_SANITIZE_STRING);
        $cart_id = filter_var(Tools::getValue('cart_id'), FILTER_SANITIZE_STRING);
        if (!is_numeric($cart_id)) {
            return $this->errorOutput('unvalid order_id');
        }
        $this->api = new PayzosApi();
        $api_response = $this->api->getPayment($payment_id);
        if (!$api_response) {
            $this->errorOutput('wrong payment');
        }
        if (!isset($api_response['status']) || $api_response['status'] !== 'approved') {
            $this->errorOutput('wrong payment');
        }
        // make payment status success
        $cart = new Cart((int) $cart_id);
        $customer = new Customer((int) $cart->id_customer);
        $payment_status = Configuration::get('PS_OS_PAYMENT');
        $message = "Payzos payment id : {$payment_id}";
        $module_name = 'Payzos';
        $currency_id = $cart->id_currency;
        list($total) = explode(' ', $api_response['real_amount']);
        $total = (float) $total;
        error_log($total);
        $validation = $this->module->validateOrder(
            (int) $cart_id,
            $payment_status,
            (float) $total,
            $module_name,
            $message,
            [
                'transaction_id' => isset($api_response['transaction_hash'])
                    ? $api_response['transaction_hash']
                    : $payment_id,
            ],
            $currency_id,
            false,
            $customer->secure_key
        );
        if (!$validation) {
            $this->errorOutput(
                "Error while proccess payment. Contact to Admin ( Payment id : {$payment_id} )"
            );
        }
        $order_id = Order::getIdByCartId((int) $cart_id);

        header('Content-type: application/json');
        header('Access-Control-Allow-Origin: *');
        // redirect user to prestashop order confirmation page
        echo json_encode([
            'redirect_url' => $this->context->link->getBaseLink() .
                'index.php?controller=order-confirmation&id_cart=' .
                (int) $cart->id .
                '&id_module=' .
                (int) $this->module->id .
                '&id_order=' .
                $order_id,
        ]);
        exit(200);
    }
}
